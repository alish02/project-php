<?php
session_start();
if (!isset($_SESSION['user'])) {
    header('Location: /login.php');
}
require_once __DIR__ . '/app/require.php';
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <center>
        <form action="/action/tickets/store.php" enctype="multipart/form-data" method="post"><br><br>
            <input type="text" placeholder="Title" name="title"><br>
            <input type="file" name="image"><br>
            <textarea name="description" placeholder="Description"></textarea><br><br>
            <button type="submit">add ticket</button>
        </form>
    </center>
</body>
</html>
