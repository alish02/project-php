<?php
session_start();
if (!isset($_SESSION['user'])) {
    header('Location: /login.php');
}

require_once __DIR__ . '/app/require.php';

if (!empty($_GET['title'])) {
    $title = filter_input(INPUT_GET, 'title');
    $query = $db->prepare("SELECT * FROM `tickets` WHERE (user_id = :user_id) AND (title LIKE :title) ORDER BY id DESC");
    $query->execute([
            'user_id' => $_SESSION['user'],
            'title'   => "%{$title}%",
    ]);
} else {
    $query = $db->prepare("SELECT * FROM `tickets` WHERE user_id = :user_id ORDER BY id DESC");
    $query->execute(['user_id' => $_SESSION['user']]);
}
$tickets = $query->fetchAll(PDO::FETCH_ASSOC);
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<center>
    <table>
        <thead>
            <?php
                $tags = $db->query("SELECT * FROM `tickets_tags`")->fetchAll(PDO::FETCH_ASSOC);

                foreach ($tickets as $ticket) {
                    $tag = array_filter($tags, function ($item) use ($ticket) {
                        return $item['id'] == $ticket['tag_id'];
                    });
                    $tag = array_shift($tag);
                    ?>
                    <tr>
                        <td><img src="<?= $ticket['image']?>" alt=""></td>
                        <td><?= $ticket['title']?></td>
                        <td style="background: <?= $tag['background']?>; color: <?= $tag['color']?>"><?= $tag['label']?></td>
                        <td><?= $ticket['description']?></td>
                        <td><?= $ticket['created_at']?></td>
                        <td>
                            <form action="/action/tickets/remove.php" method="post">
                                <input type="hidden" name="id" value="<?= $ticket['id']?>">
                                <button type="submit">delete</button>
                            </form>
                        </td>
                    </tr>
                <?}
            ?>
        </thead>
    </table>
</center>
</body>
</html>
