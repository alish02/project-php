<?php
session_start();
require_once __DIR__ . '/app/require.php';

if (isset($_SESSION['error']))
    $error = $_SESSION['error'];
    unset($_SESSION['error']);
?>

<form action="/action/user/register.php" method="post">
    <input type="text" name="name" placeholder="name" value="<?= $error['name']['value'] ?? ''?>"><br>
    <?=  $error['name']['message'] ?? ''?><br>
    <input type="text" name="email" placeholder="email" value="<?= $error['email']['value'] ?? ''?>"><br>
    <?=  $error['email']['message'] ?? ''?><br>
    <input type="text" name="date_birth" placeholder="date_birth" value="<?= $error['birth']['value'] ?? ''?>"><br>
    <?=  $error['birth']['message'] ?? ''?><br>
    <input type="password" name="pass" placeholder="pass" value="<?= $error['pass']['value'] ?? ''?>"><br>
    <?=  $error['pass']['message'] ?? ''?><br>
    <button type="submit">send</button>
</form>