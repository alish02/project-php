<?php
session_start();
require_once __DIR__ . '/../../app/require.php';
$config = require_once __DIR__ . '/../../config/app.php';
if (!isset($_SESSION['user'])) die('Error handle action');

$tagId = filter_input(INPUT_POST, 'tag');
$id    = filter_input(INPUT_POST, 'id');

$queryTag = $db->prepare("SELECT * FROM `tickets_tags` WHERE id = :id");
$queryTag->execute(['id' => $tagId]);
$tagExist = $queryTag->fetch();

if (!$tagExist) die('Error handle action');

$query = $db->prepare("SELECT `group_id` FROM `users` where id = :id");
$query->execute(['id' => $_SESSION['user']]);
$user = $query->fetch(PDO::FETCH_ASSOC);

if ($user['group_id'] === $config['admin_user_group']) {
    $queryTag = $db->prepare("UPDATE `tickets` SET tag_id = :tag_id WHERE id = :id");
    $queryTag->execute([
        'id'     => $id,
        'tag_id' => $tagId,
    ]);
}

header('Location: /ticket-control.php');