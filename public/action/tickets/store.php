<?php
session_start();
require_once __DIR__ . '/../../app/require.php';
$config = require_once __DIR__ . '/../../config/app.php';
if (!isset($_SESSION['user'])) die('Error handle action');

$title = filter_input(INPUT_POST, 'title');
$description = filter_input(INPUT_POST, 'description');
$image = $_FILES['image'];
$path = __DIR__ . '/../../upload';

$filename = uniqid().'-'.$image['name'];
move_uploaded_file($image['tmp_name'], "$path/$filename");

$query = $db->prepare("INSERT INTO `tickets` (title, description, image, tag_id, user_id) VALUES ( :title, :description, :image, :tag_id, :user_id)");
try {
    $query->execute([
        'title'       => $title,
        'image'       => "upload/{$filename}",
        'tag_id'      => $config['default_tag_id'],
        'user_id'     => $_SESSION['user'],
        'description' => $description,
    ]);
    header('Location: /my-tickets.php');
    die();
} catch (\PDOException $exception) {
    echo $exception->getMessage();
}