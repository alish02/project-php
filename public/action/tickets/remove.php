<?php
session_start();
require_once __DIR__ . '/../../app/require.php';
$config = require_once __DIR__ . '/../../config/app.php';
if (!isset($_SESSION['user'])) die('Error handle action');

$id = filter_input(INPUT_POST, 'id');
$query = $db->prepare("SELECT user_id FROM `tickets` WHERE id = :id");
$query->execute(['id' => $id]);
$ticket = $query->fetch(PDO::FETCH_ASSOC);


$queryUser = $db->prepare("SELECT * FROM `users` WHERE id = :id");
$queryUser->execute(['id' => $_SESSION['user']]);
$user = $queryUser->fetch(PDO::FETCH_ASSOC);


if ($ticket['user_id'] !== $_SESSION['user'] && $user['group_id'] !== $config['admin_user_group']) die('Error handle action');

$query = $db->prepare("DELETE FROM `tickets` WHERE id = :id");
$query->execute(['id' => $id]);
header('Location: /my-tickets.php');