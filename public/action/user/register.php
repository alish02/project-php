<?php
session_start();
require_once __DIR__ . '/../../app/require.php';

$name  = filter_input(INPUT_POST, 'name');
$email = filter_input(INPUT_POST, 'email');
$birth = filter_input(INPUT_POST, 'date_birth');
$pass  = filter_input(INPUT_POST, 'pass');
$config = require_once __DIR__ . '/../../config/app.php';


$error = [
    'name' => [
        'value'   => $name,
    ],
    'email' => [
        'value'   => $email,
    ],
    'birth' => [
        'value'   => $birth,
    ],
    'pass' => [
        'value'   => $pass,
    ],
];

if (empty($name)) {
    $error['name']['message'] = 'The name is required';
    $showError = true;
} elseif (empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $error['email']['message'] = 'The email is required';
    $showError = true;
} elseif (empty($birth)) {
    $error['birth']['message'] = 'The birth date is required';
    $showError = true;
} elseif (empty($pass)) {
    $error['pass']['message'] = 'The password is required';
    $showError = true;
} else {
    $showError = false;
}

if ($showError) {
    $_SESSION['error'] = $error;
    header('Location: /register.php');
    die();
}

$query = $db->prepare("INSERT INTO `users` (id, email, name, 	birth_date, password, group_id) VALUES (NULL, :email, :name, :birth, :pass, :group)");

try {
    $query->execute([
        'name'  => $name,
        'email' => $email,
        'birth' => $birth,
        'pass'  => password_hash($pass, PASSWORD_DEFAULT),
        'group' => $config['default_user_group']
    ]);

    header('Location: /login.php');
} catch (\PDOException $exception) {
    echo $exception->getMessage();
}