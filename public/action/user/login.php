<?php
session_start();
require_once __DIR__ . '/../../app/require.php';

$email = filter_input(INPUT_POST,'email');
$pass = filter_input(INPUT_POST,'pass');

$query = $db->prepare("SELECT * FROM `users` WHERE email = :email");
$query->execute(['email' => $email]);
$user = $query->fetch(PDO::FETCH_ASSOC);

if (!$user) {
    echo "not found user";
    die();
}
$verifyPass = password_verify($pass, $user['password']);

if (!$verifyPass) {
    echo "user error";
}

$_SESSION['user'] = $user['id'];
header('Location: /');