<?php
session_start();
require_once __DIR__ . '/app/require.php';

if (isset($_SESSION['user'])) {
    $query = $db->prepare("SELECT * FROM `users` WHERE id = :id");
    $query->execute(['id' => $_SESSION['user']]);
    $user = $query->fetch(PDO::FETCH_ASSOC);
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <center>
        <h3><?= isset($user) ? $user['name'] : 'Account'?></h3>
    </center>
    <?php
        if (isset($user)) {
            echo <<<HTML
            <form action="/action/user/logout.php" method="post">
                <button type="submit">Exit</button>
            </form>
            HTML;
        } else {
            echo <<<HTML
            <a href="/register.php">Register</a>
            <a href="/login.php">Login</a>
            HTML;
        }
    ?>
</body>
</html>
