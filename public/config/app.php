<?php

return [
    'admin_user_group'   => 1,
    'default_user_group' => 2,
    'default_tag_id'     => 3,
    'approved_tag_id'    => 1,
    'progress_tag_id'    => 2,
    'success_tag_id'     => 3,
];