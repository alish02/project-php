<?php
session_start();
require_once __DIR__ . '/app/require.php';
$config = require_once __DIR__ . '/config/app.php';
if (isset($_SESSION['user'])) {
    $query = $db->prepare("SELECT `group_id` FROM `users` where id = :id");
    $query->execute(['id' => $_SESSION['user']]);
    $user = $query->fetch(PDO::FETCH_ASSOC);
    if ((int) $user['group_id'] !== $config['admin_user_group']) {
        header('Location: /');
        die();
    }
}

$tickets = $db->query("SELECT * FROM `tickets`")->fetchAll(PDO::FETCH_ASSOC);
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<center>
    <table>
        <thead>
        <?php
        $tags = $db->query("SELECT * FROM `tickets_tags`")->fetchAll(PDO::FETCH_ASSOC);

        foreach ($tickets as $ticket) {
            $tag = array_filter($tags, function ($item) use ($ticket) {
                return $item['id'] == $ticket['tag_id'];
            });
            $tag = array_shift($tag);
            ?>
            <tr>
                <td><img src="<?= $ticket['image']?>" alt=""></td>
                <td><?= $ticket['title']?></td>
                <td style="background: <?= $tag['background']?>; color: <?= $tag['color']?>"><?= $tag['label']?></td>
                <td><?= $ticket['description']?></td>
                <td><?= $ticket['created_at']?></td>
                <td>
                    <form action="/action/tickets/change_tag.php" method="post">
                        <input type="hidden" name="id" value="<?= $ticket['id']?>">
                        <input type="hidden" name="tag" value="<?= $config['approved_tag_id']?>">
                        <button type="submit">Выполнено</button>
                    </form>
                </td>
                <td>
                    <form action="/action/tickets/change_tag.php" method="post">
                        <input type="hidden" name="id" value="<?= $ticket['id']?>">
                        <input type="hidden" name="tag" value="<?= $config['progress_tag_id']?>">
                        <button type="submit">В процессе</button>
                    </form>
                </td>
                <td>
                    <form action="/action/tickets/change_tag.php" method="post">
                        <input type="hidden" name="id" value="<?= $ticket['id']?>">
                        <input type="hidden" name="tag" value="<?= $config['success_tag_id']?>">
                        <button type="submit">Создано</button>
                    </form>
                </td>
                <td>
                    <form action="/action/tickets/remove.php" method="post">
                        <input type="hidden" name="id" value="<?= $ticket['id']?>">
                        <button type="submit">delete</button>
                    </form>
                </td>
            </tr>
        <?}
        ?>
        </thead>
    </table>
</center>
</body>
</html>
